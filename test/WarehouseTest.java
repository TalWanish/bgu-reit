import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class WarehouseTest {

	private Warehouse wh;
	
	@Before
	public void setUp() throws Exception {
		wh=new Warehouse();
		wh.addTool(new RepairTool("Hammer",1));
		wh.addMaterial(new RepairMaterial("Nails",2000));
	}



	@Test
	public void testAddTool() {
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testAddMaterial() {
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void testRequestTool() {
		RepairToolInformation tool=new RepairToolInformation("Screwdriver",3);
		try{
			wh.requestTool(tool);
		}
	 catch (Exception e) {
		String ans="The requested tool does not exist in the Warehouse";
		assertEquals(ans,e.getMessage());	
	 	}	
	}
	@Test
	public void testReleaseTool() {
		fail("Not yet implemented");
	}

	@Test
	public void testRequestMaterials() {
		RepairMaterialInformation lis=new RepairMaterialInformation("Nails",2001);
		try{
			wh.requestMaterial(lis);
		}
		
		catch (Exception e) {
		String ans="There isn't enough material for you";
		assertEquals(ans,e.getMessage());	
	 	}
	}

	@Test
	public void testUpdateToolStatistics() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdateMaterialStatistics() {
		fail("Not yet implemented");
	}

	
	@After
	public void tearDown() throws Exception {
	}

}
