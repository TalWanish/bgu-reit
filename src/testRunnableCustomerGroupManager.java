import java.util.LinkedList;


public class testRunnableCustomerGroupManager {
	
	private static Warehouse fWarehouse=new Warehouse();
	private static AssetContentsRepairInformation fAssetContentsRepairInformation=new AssetContentsRepairInformation();
	private static Management fManagement=new Management(fWarehouse,fAssetContentsRepairInformation);
	private static Statistics fStatistics=new Statistics(fManagement) ;
	
	
	
	
	
	
	
	
	
	
	public static void main(String[] args) {
		
		fWarehouse.addMaterial(new RepairMaterial("Nails",20000,fStatistics));
		fWarehouse.addTool(new RepairTool("Hammer",2,fStatistics));
		fWarehouse.addTool(new RepairTool("Screwdiver",4,fStatistics));
		
		AssetContentRepairDetails tv=new AssetContentRepairDetails("TV");
		tv.addRepairMaterialInformation("Nails", 1000);
		tv.addRepairToolInformation("Hammer", 1);
		
		AssetContentRepairDetails stove=new AssetContentRepairDetails("Stove");
		stove.addRepairMaterialInformation("Nails", 1500);
		stove.addRepairToolInformation("Screwdiver", 1);
		stove.addRepairToolInformation("Hammer", 2);

		
		fAssetContentsRepairInformation.addAssetContentRepairDetails(tv);
		fAssetContentsRepairInformation.addAssetContentRepairDetails(stove);
		
		LinkedList<AssetContent> fDamagedContentsListOfHat=new LinkedList<AssetContent>();
		fDamagedContentsListOfHat.add(new AssetContent("TV", 3));
		LinkedList<AssetContent> fDamagedContentsListOfFlat=new LinkedList<AssetContent>();
		fDamagedContentsListOfFlat.add(new AssetContent("Stove", 3));
		
		
		Asset hat=new Asset("hat","hat",fDamagedContentsListOfHat,3);
		Asset flat=new Asset("flat","flat",fDamagedContentsListOfFlat,3);
		
		fManagement.addAsset(hat);
		fManagement.addAsset(flat);

		
		CustomerGroupDetails CustomerGroupDetailsFirst=new CustomerGroupDetails("John");
		CustomerGroupDetails CustomerGroupDetailsSecond=new CustomerGroupDetails("Turing");
		
		CustomerGroupDetailsFirst.addCustomer(new Customer("Omri", "ARBITRARY", 3, 7));
		CustomerGroupDetailsFirst.addCustomer(new Customer("Lev", "FIXED", 1, 4));
		CustomerGroupDetailsFirst.addCustomer(new Customer("BenAmi", "NONE", 2, 4));
		
		CustomerGroupDetailsFirst.addRentalRequest(new RentalRequest(1, "hat", 3, 3));

		CustomerGroupDetailsSecond.addCustomer(new Customer("Albina", "ARBITRARY", 2, 3));
		CustomerGroupDetailsSecond.addCustomer(new Customer("Tanya", "FIXED", 8, 20));
		CustomerGroupDetailsSecond.addCustomer(new Customer("Irena", "NONE", 3, 4));
		
		CustomerGroupDetailsSecond.addRentalRequest(new RentalRequest(0, "flat", 3, 2));
		
		RunnableCustomerGroupManager John=new RunnableCustomerGroupManager("John", CustomerGroupDetailsFirst, fManagement); 
		RunnableCustomerGroupManager Turing=new RunnableCustomerGroupManager("Turing", CustomerGroupDetailsSecond, fManagement);
		
		Thread th1=new Thread(John);
		Thread th2=new Thread(Turing);


		th1.start();
		th2.start();


		
	}

}
