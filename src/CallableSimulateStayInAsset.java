import java.util.concurrent.Callable;


public class CallableSimulateStayInAsset implements Callable<Double> {
	
	private Customer fCustomer;

	public CallableSimulateStayInAsset(Customer fCustomer) {
		this.fCustomer=fCustomer;
	}

	@Override
	public Double call() throws Exception {
		return fCustomer.getCustomerDamage();
	}
	
	

}
