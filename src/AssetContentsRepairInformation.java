import java.util.concurrent.ConcurrentHashMap;


public class AssetContentsRepairInformation {
	
	private ConcurrentHashMap<String, AssetContentRepairDetails> fAssetContentRepairDetails;
	
	public AssetContentsRepairInformation (){
		fAssetContentRepairDetails=new ConcurrentHashMap<String, AssetContentRepairDetails>();
	}
	
	public void addAssetContentRepairDetails(AssetContentRepairDetails toBeAdded){
		fAssetContentRepairDetails.put(toBeAdded.getAssentContentName(), toBeAdded);

	}
	
	public AssetContentRepairDetails getAssetContentRepairDetails(String assetContentName){
		return fAssetContentRepairDetails.get(assetContentName);
	}



}
