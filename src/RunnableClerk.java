import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

public class RunnableClerk implements Runnable {

	private Management fManagement;
	private String fName;
	private Location fLocation;
	private int fRequestsHandled;
	private RentalRequest fCurrentRentalRequest;
	private int fWorkTime;
	private boolean fShiftOver;
	private CyclicBarrier fCyclicBarrierMaintenanceRequest;

	public RunnableClerk(Management management, ClerkDetails clekDetails,
			CyclicBarrier fCountDownLatchMaintenanceRequest) {
		fManagement = management;
		fName = clekDetails.getClerkName();
		fLocation = clekDetails.getClerkLocation();
		fRequestsHandled = 0;
		this.fCyclicBarrierMaintenanceRequest = fCyclicBarrierMaintenanceRequest;

	}

	// Asks management for an Asset that fits the rental request
	public Asset requestAsset(RentalRequest currentRentalRequest) {
		Asset requestedAsset = fManagement.findSuitableAsset(
				currentRentalRequest.getRentalRequestType(),
				currentRentalRequest.getRentalRequestSize());
		return requestedAsset;
	}

	public boolean clerkShiftOver() {
		return fShiftOver;
	}

	public void run() {

		while (fManagement.hasPendingRentalRequest()) {
			
			while (!fShiftOver) {
				fCurrentRentalRequest = fManagement.getRentalRequest();

				Asset suitabledAsset = fManagement.findSuitableAsset(
						fCurrentRentalRequest.getRentalRequestType(),
						fCurrentRentalRequest.getRentalRequestSize());

				if (suitabledAsset.getAssetStatus() != "AVAILABLE") {
					synchronized (suitabledAsset) {
						try {
							suitabledAsset.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}

				else {

					// update request status
					fCurrentRentalRequest
							.setAssetForRentalRequest(suitabledAsset);
					// Calculate distance to asset
					double distance = suitabledAsset
							.calculateDistance(fLocation);
					// Change status to booked
					suitabledAsset.updateAssetStatus("BOOKED");

					// Sleep the time according to distance
					try {
						Thread.sleep((long) (distance * 2000));
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					fWorkTime += distance * 2;
					fCurrentRentalRequest
							.updateRentalRequestStatus("FULFILLED");
					fRequestsHandled++;
					fCurrentRentalRequest.notifyAll();
				}
				fShiftOver = fWorkTime > 8;
			}
			
			try {
				fCyclicBarrierMaintenanceRequest.await();
			} catch (InterruptedException | BrokenBarrierException e) {
				e.printStackTrace();
			}

		}

	}

	public String getClerkName() {
		return fName;
	}

}
