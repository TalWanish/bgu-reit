import java.util.concurrent.CountDownLatch;


public class BarrierAction implements Runnable {
	
	private CountDownLatch fCountDownLatchMaintenanceRequest;
	private Management fManagement;
	
	public BarrierAction(CountDownLatch fCountDownLatchMaintenanceRequest,Management fManagement){
		this.fCountDownLatchMaintenanceRequest=fCountDownLatchMaintenanceRequest;
		this.fManagement=fManagement;
	}

	@Override
	public void run() {
		
		try {
			fCountDownLatchMaintenanceRequest.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		this.fManagement.resetCyclicBarrier();
		
		
	}
	

}
