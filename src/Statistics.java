import java.util.ArrayList;


public class Statistics {
	
	private double fMoneyGained;
	private Management fManagement;
	private ArrayList<Statistical> fStatistics=new ArrayList<Statistical>();
	
	public Statistics(Management fManagement){
		this.fManagement=fManagement;
	}
	
	public void addStatisticalItem(Statistical toBeAdded){
		fStatistics.add(toBeAdded);
	}
	
	public String getStatistics(){
		String ans="";
		for(Statistical it: fStatistics)
			ans=ans+it.getItemStatistcs()+'\n';
		
		return ans;
	}
}
