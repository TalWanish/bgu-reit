
public enum AssetStatus{
	
	AVAILABLE("AVAILABLE"),
	BOOKED("BOOKED"),
	OCCUPIED("OCCUPIED"),
	UNAVAILABLE("UNAVAILABLE");
	
	private final String fAssetStatus;
	
	AssetStatus(String status){
		fAssetStatus = status;
	}
	
	public String getAssetStatus() {
		return fAssetStatus;
	}
	
}
