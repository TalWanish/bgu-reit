import java.util.LinkedList;


public interface WarehouseInterface {
	
	/**
	 * 
	 * Will try to acquire the given amount of tools.
	 * 
	 * @param repairToolInformation
	 * @throws Exception
	 * 			in case the tool does not exist in the warehouse.
	 * @pre: fTotalQuantity=>repairToolInformation.getQuantity().
	 * @inv: there are always enough tools to perform a single repair task.
	 * @post: RepairTool.fCurrentUsedQuantity=@pre(RepairTool.fCurrentUsedQuantity)+repairToolInformation.getQuantity()
	 *
	 */
	public void requestTool(RepairToolInformation repairToolInformation) throws Exception;
	/**
	 * Will add the given tool to the warehouse.
	 * @param a
	 * @pre: the tool to be added does not exist in the warehouse.
	 * @post: the size of fToolsMap was increased by one
	 */
	public void addTool(RepairTool a);
	/**
	 * Will add the given material to the warehouse.
	 * @param a
	 * @pre: the material to be added does not exist in the warehouse.
	 * @post: the size of fMaterialsMap was increased by one
	 */
	
	public void addMaterial(RepairMaterial a);

	

	/**
	 * Will update decrease the amount of currently used tools by repairToolInformation.getQuantity() 
	 * @param repairToolInformation
	 * @pre:RepairTool.fCurrentUsedQuantity >=  repairToolInformation.getQuantity()  
	 * @post: RepairTool.fCurrentUsedQuantity=@pre(RepairTool.fCurrentUsedQuantity)-repairToolInformation.getQuantity()
	 * 			
	 */
	public void releaseTool(RepairToolInformation repairToolInformation);	
	/**
	 * 
	 * Will try to acquire the given amount of material.
	 * 
	 * @param repairMaterialInformation
	 * @throws Exception
	 * 			in case the material does not exist in the warehouse.
	 * @pre: fTotalQuantity=>repairMaterialInformation.getQuantity().
	 * @inv: there are always enough material to perform a single repair task.
	 * @post: RepairMaterial.fCurrentUsedQuantity=@pre(RepairMaterial.fCurrentUsedQuantity)-repairMaterialInformation.getQuantity()
	 *
	 */
	public void requestMaterial(RepairMaterialInformation repairMaterialInformation) throws Exception;
	
	public void updateToolStatistics(RepairToolInformation a);
	
	public void updateMaterialStatistics(RepairMaterialInformation a);
		
}
