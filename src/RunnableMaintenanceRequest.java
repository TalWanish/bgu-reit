import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;


public class RunnableMaintenanceRequest implements Runnable {
	
	private Asset fAsset;
	
	private String fMaintenanceGuyName;
	
	private Warehouse fWarehouse;
	private Management fManagement;

	private ConcurrentHashMap<String,RepairToolInformation> fMapOfRepairToolInformation=new ConcurrentHashMap<String,RepairToolInformation>();
	private ConcurrentHashMap<String,RepairMaterialInformation> fMapOfRepairMaterialInformation=new ConcurrentHashMap<String,RepairMaterialInformation>();
	
	private LinkedList<AssetContent> fListOfDamagedContents;
	
	private long timeToSleep;

	private CountDownLatch fCountDownLatchMaintenanceRequest;
		
    private static final Logger logger = Logger.getLogger(RunnableMaintenanceRequest.class.getName());

	
	public RunnableMaintenanceRequest(String fMaintenanceGuyName, Asset fAsset,Warehouse fWarehouse,CountDownLatch fCountDownLatchMaintenanceRequest, Management fManagement){
		this.fMaintenanceGuyName=fMaintenanceGuyName;
		this.fAsset=fAsset;
		this.fWarehouse=fWarehouse;
		this.fManagement=fManagement;
		fListOfDamagedContents=fAsset.retrieveListOfDamagedContents();		
		this.fCountDownLatchMaintenanceRequest=fCountDownLatchMaintenanceRequest;

	}
	

	
	private void addRepairToolInformation(RepairToolInformation toBeAdded){
		
		if(!fMapOfRepairToolInformation.containsKey(toBeAdded.getToolName()))
				fMapOfRepairToolInformation.put(toBeAdded.getToolName(), toBeAdded);
		
		else{
			RepairToolInformation current=fMapOfRepairToolInformation.get(toBeAdded.getToolName());
			
			if(current.getRequestedQuantity()<toBeAdded.getRequestedQuantity()){
				fMapOfRepairToolInformation.remove(current);
				fMapOfRepairToolInformation.put(toBeAdded.getToolName(), toBeAdded);
			}	
			
		}
	}
	
	private void addRepairMaterialInformation(RepairMaterialInformation toBeAdded) {
		if(!fMapOfRepairMaterialInformation.containsKey(toBeAdded.getMaterialName()))
			fMapOfRepairMaterialInformation.put(toBeAdded.getMaterialName(), toBeAdded);
	
	else{
		RepairMaterialInformation current=fMapOfRepairMaterialInformation.get(toBeAdded.getMaterialName());
		
		if(current.getRequestedQuantity()<toBeAdded.getRequestedQuantity()){
			fMapOfRepairMaterialInformation.remove(current);
			fMapOfRepairMaterialInformation.put(toBeAdded.getMaterialName(), toBeAdded);
		}	
		
		}		
	}
	
	public void generateListOfRepairToolInformation(){
		
		for(AssetContent itAssetContent:fListOfDamagedContents){
			AssetContentRepairDetails assetContentRepairDetails=fManagement.getAssetContentRepairDetails(itAssetContent.getAssetContentName());
			LinkedList<RepairToolInformation> listOfRequiredRepairTools=assetContentRepairDetails.getListOfRequiredRepairTools();
			for(RepairToolInformation itRepairToolInformation: listOfRequiredRepairTools)
				addRepairToolInformation(itRepairToolInformation);
		}
	}
	
	private void generateListOfRepairMaterialInformation() {
		
		for(AssetContent itAssetContent:fListOfDamagedContents){
			AssetContentRepairDetails assetContentRepairDetails=fManagement.getAssetContentRepairDetails(itAssetContent.getAssetContentName());
			LinkedList<RepairMaterialInformation> listOfRequiredRepairMaterials=assetContentRepairDetails.getListOfRequiredRepairMaterials();
			for(RepairMaterialInformation itRepairMaterialInformation: listOfRequiredRepairMaterials)
				addRepairMaterialInformation(itRepairMaterialInformation);
		}		
	}
	
	




	public void run() {
		
		generateListOfRepairToolInformation();
		generateListOfRepairMaterialInformation();
		calculateTimeToSleep();
		
		for(RepairToolInformation it : fMapOfRepairToolInformation.values()){
			System.out.println("RunnableMaintenanceRequest "+this.fMaintenanceGuyName+ " for asset "+fAsset.getAssetName()+" requests for "+it.getRequestedQuantity()+" "+ it.getToolName());
			fWarehouse.requestTool(it);
		}
		
		for(RepairMaterialInformation it : fMapOfRepairMaterialInformation.values()){
			System.out.println("RunnableMaintenanceRequest "+this.fMaintenanceGuyName+ " for asset "+fAsset.getAssetName()+" requests for "+it.getRequestedQuantity()+" "+ it.getMaterialName());
			fWarehouse.requestMaterial(it);
		}
		
		
		try {
			System.out.println("RunnableMaintenanceRequest "+this.fMaintenanceGuyName+ " for asset "+fAsset.getAssetName()+" acquired all tools and materials");
			Thread.sleep(timeToSleep);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		for(RepairToolInformation it : fMapOfRepairToolInformation.values()){
		System.out.println("RunnableMaintenanceRequest "+this.fMaintenanceGuyName+ " for asset "+fAsset.getAssetName()+" releases "+it.getRequestedQuantity()+" "+ it.getToolName());
			fWarehouse.releaseTool(it);
		}
		
		System.out.println("RunnableMaintenanceRequest "+this.fMaintenanceGuyName+ " for asset "+fAsset.getAssetName()+" released all tools");
		
		markAssetContensAsFixed();
		this.fAsset.markAssetAsFix();
		this.fAsset.updateAssetStatus("AVAILABLE");	
		fCountDownLatchMaintenanceRequest.countDown();
		
	}

	private void markAssetContensAsFixed() {	
		for(AssetContent it: fListOfDamagedContents)
			it.markAssetContentsAsFix();	
	}

	private void calculateTimeToSleep() {
		
		for(AssetContent it: fListOfDamagedContents)
			this.timeToSleep=timeToSleep+(long)it.getRepairTime();
	}

}
