import java.util.concurrent.atomic.AtomicInteger;


public class RepairMaterial {
	
	private String fName;
	private  AtomicInteger fTotalQuantity;
	private RepairToolMaterialStatistics fRepairMaterialStatistics;
	private Statistics fStatistics;
	public RepairMaterial(){
		fName="";
	}
	
	public RepairMaterial(String fName,int fTotalQuantity,Statistics fStatistics){
		this.fName=fName;
		this.fRepairMaterialStatistics=new RepairToolMaterialStatistics(this.fName,fTotalQuantity);
		this.fStatistics=fStatistics;
		this.fStatistics.addStatisticalItem(this.fRepairMaterialStatistics);
		this.fTotalQuantity=new AtomicInteger(fTotalQuantity);
	}
	
	public void acquireMaterial(int requstedQuantity){
		System.out.println("RepairMaterial acquireMaterial:"+fName+" requstedQuantity is "+requstedQuantity);
		fTotalQuantity.set(fTotalQuantity.get()-requstedQuantity);
		fRepairMaterialStatistics.updateStatistcs("aquired "+requstedQuantity+" ");
		System.out.println("RepairMaterial acquireMaterial:"+fName+" current quantity after acquired is "+fTotalQuantity.get());

	}
	
	public String getName() {
		return this.fName;
	}
	
}
