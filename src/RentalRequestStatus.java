
enum RentalRequestStatus {
	

	INCOMPLETE("INCOMPLETE"),
	FULFILLED("FULFILLED"),
	INPROGRESS("INPROGRESS"),
	COMPLETE("COMPLETE");
	
	private final String fRentalRequestStatus;
	
	RentalRequestStatus(String status){
		fRentalRequestStatus=status;
	}
	
	public String getRentalRequestStatus(){
		return fRentalRequestStatus;
	}

}
