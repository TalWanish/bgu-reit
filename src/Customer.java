
public class Customer {

	
	private String fCustomerName;
	private double fMinimum;
	private double fMaximum;
	private VandalismStatus fVandalismStatus;

	public Customer(String customerName, String vandalismType,double minimum, double maximum) {
		fCustomerName = customerName;
		fMinimum = minimum;
		fMaximum = maximum;
		setVandalismStatus(vandalismType);
	}
	
	private void setVandalismStatus(String vandalismType){
		
		switch(vandalismType){
		case "ARBITRARY":
			fVandalismStatus=VandalismStatus.ARBITRARY;
			break;
		case "FIXED":
			fVandalismStatus=VandalismStatus.FIXED;
			break;
		case "NONE":
			fVandalismStatus=VandalismStatus.NONE;
			break;
		}
				
		}
		
	private String getVandalismType(){
		return fVandalismStatus.getVandalismType();
	}
	
	public double getCustomerDamage(){
		
		switch(getVandalismType()){
		case "ARBITRARY":
			return fMinimum+Math.random()*fMaximum;
		case "FIXED":
			return (this.fMinimum+this.fMaximum)/2;
	
		}
		
		return 1/2;
		
	}


}