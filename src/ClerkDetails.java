public class ClerkDetails {
	
	private String fName;
	private Location fLocation;

	ClerkDetails(String name, Location location) {
		fName = name;
		fLocation = location;
	}

	public String toString() {
		return "Clerk Name: "+fName+" Location: "+fLocation.toString();
	}
	
	public String getClerkName(){
		return this.fName;
	}

	public Location getClerkLocation() {
		return this.fLocation;
	}
}
