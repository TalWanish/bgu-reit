
public class DamageReport {
	
	private Asset fAsset;
	private String fAssetName;
	private double fTotalDamage;
	
	public DamageReport(Asset fAsset, double fTotalDamage) {
		this.fAsset=fAsset;
		this.fTotalDamage=fTotalDamage;
		this.fAssetName=fAsset.getAssetName();
		}
	
	public String getAssetName(){
		return fAssetName;
	}
	
	public Asset getAsset(){
		return fAsset;
	}
	
	public double getDamage(){
		return this.fTotalDamage;
	}
	
	

}
