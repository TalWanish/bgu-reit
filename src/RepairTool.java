import java.util.concurrent.Semaphore;


public class RepairTool {
	
	private String fName;
	private RepairToolMaterialStatistics fRepairToolStatistics;
	private Semaphore sem;
	private Statistics fStatistics;
	
	
	public RepairTool(String fName,int fTotalQuantity,Statistics fStatistics){
		this.fName=fName;
		this.fStatistics=fStatistics;
		this.fRepairToolStatistics=new RepairToolMaterialStatistics(fName,fTotalQuantity);
		this.fStatistics.addStatisticalItem(this.fRepairToolStatistics);
		this.sem=new Semaphore(fTotalQuantity);
	}
	
	

	




	public String getRepairToolName() {
		return this.fName;
	}

	
	public void acquireTool(int requstedQuantity){
		
		
		System.out.println("RepairTool acquireTool:"+fName+" requstedQuantity is "+requstedQuantity);
		System.out.println("RepairTool acquireTool:"+fName+" current Availeble is "+sem.availablePermits());

			try {
				sem.acquire(requstedQuantity);
				fRepairToolStatistics.updateStatistcs("aquired "+requstedQuantity+" ");
				System.out.println("RepairTool acquireTool:"+fName+" current Availeble is "+sem.availablePermits());
			}
			
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		
	}
	
	public void releaseTool(int requstedQuantity){
		System.out.println("RepairTool releaseTool:"+fName+" toBeReleasedQuantity is "+requstedQuantity);
		System.out.println("RepairTool releaseTool:"+fName+" current Available is "+sem.availablePermits());
		sem.release(requstedQuantity);
		System.out.println("RepairTool releaseTool:"+fName+" after released current Available is "+sem.availablePermits());
		}

	
	
}
