
public class Location {
	private double x;
	private double y;
	
	public Location(double x,double y){
		this.x=x;
		this.y=y;
	}
	

	double calculateDistance(Location other){
		double xDis=0,yDis=0;
		xDis=Math.pow(Math.abs(other.x-this.x), 2);
		yDis=Math.pow(Math.abs(other.y-this.y), 2);
		
		return Math.sqrt(xDis+yDis);

	}
	

}
