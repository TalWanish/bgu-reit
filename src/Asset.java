import java.util.LinkedList;
import java.util.Vector;


public class Asset {
	
	private String fName;
	private String fType;
	private Location fLocation; 
	private AssetStatus fAssetStatus;
	private double fHealth;
	private double fCostPerNight;
	private Integer fSize;
	private LinkedList<AssetContent> fAssetContentsList=new LinkedList<AssetContent> ();
	
	//temporary Constructor
	public Asset(String fName,LinkedList<AssetContent> fDamagedContentsList){
		this.fName=fName;
		this.fAssetContentsList=fDamagedContentsList;
	}


	public Asset(String name, String type, Location assetLocation,double costPerNight, int size) {
		this.fName=name;
		this.fCostPerNight=costPerNight;
		this.fLocation=assetLocation;
		this.fSize=size;
		this.fHealth=100;
		updateAssetStatus("AVAILABLE");	
	}

	public Asset(String fName,String fType,LinkedList<AssetContent> fDamagedContentsList, int fSize) {
		this.fName=fName;
		this.fType=fType;
		this.fAssetContentsList=fDamagedContentsList;
		this.fSize=fSize;
		this.fHealth=100;
		this.fCostPerNight=30;
	}


	public void addAssetContent(AssetContent toBeAdded){
		fAssetContentsList.add(toBeAdded);
	}

	public LinkedList<AssetContent> retrieveListOfDamagedContents() {
		
		LinkedList<AssetContent> listOfDamagedContents=new LinkedList<AssetContent>();
		for(AssetContent it: this.fAssetContentsList){
			//if(it.getAssetContentHealth()<65)
				listOfDamagedContents.add(it);
			}
		
		return listOfDamagedContents;
	}


	public String getAssetName() {
		return fName;
}


	public void updateAssetStatus(String status) {
			switch(status){
			case "AVAILABLE":
				fAssetStatus=AssetStatus.AVAILABLE;
				break;
			case "BOOKED":
				fAssetStatus=AssetStatus.BOOKED;
				break;
			case "OCCUPIED":
				fAssetStatus=AssetStatus.OCCUPIED;
				break;
			case "UNAVAILABLE":
				fAssetStatus=AssetStatus.UNAVAILABLE;
				break;
					
			}
		}

	public double getAssetHealth(){
		return this.fHealth;
	}
	
	public String getAssetStatus(){
        return fAssetStatus.getAssetStatus();
	}
	
    public double calculateDistance(Location other){
    	return fLocation.calculateDistance(other);
    }
	
	public void markAssetAsFix() {
		fHealth=100;		
	}


	public Integer getAssetSize() {
		return fSize;
	}


	public void updateHealth(double damage) {
		this.fHealth=damage;
	}		



}
