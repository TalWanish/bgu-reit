import java.util.Vector;


public class RepairToolInformation {
	String fToolName;
	int fQuantity;
	
	public RepairToolInformation(){
		fToolName="";
		fQuantity=0;
	}
	
	public RepairToolInformation(String fToolName,int fQuantity){
		this.fToolName=fToolName;
		this.fQuantity=fQuantity;
	}

	public RepairToolInformation(String contentName,Vector<RepairTool> vectorRepairTool) {
		// TODO Auto-generated constructor stub
	}

	public String getToolName() {
		return fToolName;
	}

	public int getRequestedQuantity() {
		return fQuantity;
	}

	
	
	

}
