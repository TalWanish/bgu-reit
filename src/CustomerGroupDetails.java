import java.util.ArrayList;
import java.util.Iterator;

public class CustomerGroupDetails {
	
	private String fGroupManagerName;
	private ArrayList<RentalRequest> fRentalRequests;
	private ArrayList<Customer> fCustomers;

	public CustomerGroupDetails(String managerName) {
		fRentalRequests = new ArrayList<RentalRequest>();
		fCustomers = new ArrayList<Customer>();
		fGroupManagerName = managerName;
	}
	
	public void addCustomer(Customer customer){
		fCustomers.add(customer);
	}
	
	public void addRentalRequest(RentalRequest rentalRequest){
		fRentalRequests.add(rentalRequest);
	}

	public RentalRequest nextRequest(){
			return fRentalRequests.remove(fRentalRequests.size()-1);
		}

	public long getTotalRentalRequestNumber() {
		return fRentalRequests.size();
	}
	
	public int getCustomerArrayListSize() {
		return fCustomers.size();
	}

	public boolean hasRentalRequest() {
		return !fRentalRequests.isEmpty();
	}
	
	public Iterator<Customer> getCustomerArrayListIterator(){
		return this.fCustomers.iterator();
	}

	public String getGroupManagerName() {
		return fGroupManagerName;
	}
			
	
}
