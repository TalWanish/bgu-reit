
public class RentalRequest {

	private int fId;
	private String fType;
	private Integer fSize;
	private int fDuration;
	private Asset fAsset;
	private RentalRequestStatus fRentalRequestStatus;

	public RentalRequest(int id, String type, Integer size, int durationOfStay) {
		
		this.fId = id;
		this.fType = type;
		this.fSize = size;
		this.fDuration=durationOfStay;		
	}
	
	
	public void updateRentalRequestStatus(String status){
		switch(status){
		
		case "INCOMPLETE":
			fRentalRequestStatus=RentalRequestStatus.INCOMPLETE;
			break;
		case "FULFILLED":
			fRentalRequestStatus=RentalRequestStatus.FULFILLED;
			break;
		case "INPROGRESS":
			fRentalRequestStatus=RentalRequestStatus.INPROGRESS;
			break;
		case "COMPLETE":
			fRentalRequestStatus=RentalRequestStatus.COMPLETE;
			break;
				
		}
	}
	
	
	public void setAssetForRentalRequest(Asset asset){
		this.fAsset=asset;
	}


	public void updateRentalRequestAssetStatus(String status) {
		fAsset.updateAssetStatus(status);
	}


	public String getAssetName() {
		return this.fAsset.getAssetName();
	}
	
	public String getRentalRequestType(){
		return this.fType;
	}


	public Asset getRentalRequestAsset() {
		return this.fAsset;
	}


	public int getRentalRequestDuration() {
		return fDuration;
	}
	
	public String toString(){
		return "Type: "+fType+" ,Size: "+fSize+" ,Status: "+fRentalRequestStatus.getRentalRequestStatus();
	}


	public Integer getRentalRequestSize() {
		return fSize;
	}


	
	

}
