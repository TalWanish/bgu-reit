import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.LinkedBlockingQueue;

public class Management {

	private Vector<ClerkDetails> fClerkDetailsVec;
	private Warehouse fWarehouse;
	private ConcurrentHashMap<String, RepairToolInformation> fRepairToolInformationMap = new ConcurrentHashMap<String, RepairToolInformation>();
	private ConcurrentHashMap<String, RepairMaterialInformation> fRepairMaterialInformationMap = new ConcurrentHashMap<String, RepairMaterialInformation>();
	private AssetContentsRepairInformation fAssetContentsRepairInformation = new AssetContentsRepairInformation();
	private ArrayList<CustomerGroupDetails> fCustomerGroupDetailsList=new ArrayList<CustomerGroupDetails>(); 
	private Assets fAssets = new Assets();
	private Statistics fStatistics;
	private LinkedBlockingQueue<RentalRequest> fRentalRequestQueue = new LinkedBlockingQueue<RentalRequest>();
	private Vector<DamageReport> fDamageReportsVec = new Vector<DamageReport>();
	private CyclicBarrier fCyclicBarrierMaintenanceRequest;
	private CountDownLatch fCountDownLatchMaintenanceRequest;
	private BarrierAction fBarrierAction;
	private boolean fFirstIteration=true;
	private Vector<Asset> fDamagedAssetsVec;

	public Management(Warehouse fWarehouse) {
		this.fWarehouse = fWarehouse;
	}

	public Management(Warehouse fWarehouse2,
			AssetContentsRepairInformation fAssetContentsRepairInformation2) {
		this.fWarehouse = fWarehouse2;
		this.fAssetContentsRepairInformation = fAssetContentsRepairInformation2;
	}

	public void addRepairToolInformation(RepairToolInformation toBeAdded) {
		fRepairToolInformationMap.put(toBeAdded.getToolName(), toBeAdded);
	}

	public void addRepairMaterialInformation(RepairMaterialInformation toBeAdded) {
		fRepairMaterialInformationMap.put(toBeAdded.getMaterialName(),
				toBeAdded);
	}

	public void addAssetContentsRepairDetails(
			AssetContentRepairDetails toBeAdded) {
		fAssetContentsRepairInformation.addAssetContentRepairDetails(toBeAdded);
	}

	public void addClerk(ClerkDetails clerkToAdd) {
		fClerkDetailsVec.addElement(clerkToAdd);
	}

	public void addStatisticalItem(Statistical toBeAdded) {
		fStatistics.addStatisticalItem(toBeAdded);
	}

	public void addCustomerGroupDetails(CustomerGroupDetails toBeAdded){
		this.fCustomerGroupDetailsList.add(toBeAdded);
	}
	public AssetContentRepairDetails getAssetContentRepairDetails(
			String assetContentName) {
		return fAssetContentsRepairInformation
				.getAssetContentRepairDetails(assetContentName);

	}


	public Asset findSuitableAsset(String type, Integer size) {
		Asset suitableAsset = fAssets.findSuitableAsset(type, size);
		return suitableAsset;
	}

	public void submitRentalRequest(RentalRequest rentalRequest) {

		System.out.println("Management: submitRentalRequest "
				+ rentalRequest.getRentalRequestType());

		try {
			fRentalRequestQueue.put(rentalRequest);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void addDamageReport(DamageReport damageReport) {
		this.fDamageReportsVec.addElement(damageReport);
	}

	public RentalRequest getRentalRequest() {

		if (!this.fRentalRequestQueue.isEmpty()) {
			try {
				return fRentalRequestQueue.take();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public boolean hasPendingRentalRequest() {
		System.out.println("Management: hasPendingRentalRequest "
				+ fRentalRequestQueue.size());
		return !this.fRentalRequestQueue.isEmpty();

	}

	public void addAsset(Asset toBeAdded) {
		fAssets.addToAssets(toBeAdded);
	}
	
	private void createRunnableClerks() {
		for (int i = 0; i < fClerkDetailsVec.size(); i++) {
			Thread t = new Thread(new RunnableClerk(this,fClerkDetailsVec.elementAt(i),this.fCyclicBarrierMaintenanceRequest));
			System.out.println("Clerk " + fClerkDetailsVec.elementAt(i).getClerkName() + "is on his way! ");
			t.start();
		}
	}
	
	private void createRunnableCustomerGroupManager(){

		for (int i = 0; i < this.fCustomerGroupDetailsList.size(); i++) {
			Thread t = new Thread(new RunnableCustomerGroupManager(this.fCustomerGroupDetailsList.get(i).getGroupManagerName(), this.fCustomerGroupDetailsList.get(i), this));
			System.out.println("RunnableCustomerGroupManager " + this.fCustomerGroupDetailsList.get(i).getGroupManagerName() + "is on his way! ");
			t.start();
		}
		
	}
	
	
	
	
	public void runSimulation(){
		fCyclicBarrierMaintenanceRequest=new CyclicBarrier(fClerkDetailsVec.size(), fBarrierAction);
		createRunnableCustomerGroupManager();
		int countShifts=1;
		while(hasPendingRentalRequest()){
			
			if(this.fFirstIteration){
				createRunnableClerks();
				this.fFirstIteration=false;	
			}

			while(fCyclicBarrierMaintenanceRequest.getNumberWaiting()>0){
				System.out.println("Shift"+countShifts+" is not over yet");	
			}
			
			System.out.println("Shift"+countShifts+" is over");
			
			updateAssetsHealth();
			
			this.fDamagedAssetsVec=this.fAssets.retrieveDamagedAssetsVec();
	
			activateCountDownLatchMaintenanceAndBarrierAction();
			
			createRunnableMaintenanceRequest();
			
			}
		
	}
	
	private void updateAssetsHealth() {
		for(DamageReport it: this.fDamageReportsVec){
			this.fAssets.updateAssetsHealth(it.getAsset(),it.getDamage());
		}
		
	}

	private void createRunnableMaintenanceRequest() {
		int counter=0;
		for(Asset it: this.fDamagedAssetsVec){
			Thread t=new Thread(new RunnableMaintenanceRequest("NO"+counter,it,this.fWarehouse,this.fCountDownLatchMaintenanceRequest,this));
			t.start();
		}
		
	}

	private void activateCountDownLatchMaintenanceAndBarrierAction(){
		this.fCountDownLatchMaintenanceRequest=new CountDownLatch(fDamagedAssetsVec.size());
		this.fBarrierAction=new BarrierAction(fCountDownLatchMaintenanceRequest, this);
		Thread thBarrierAction=new Thread(fBarrierAction);
		thBarrierAction.run();
	}

	public void resetCyclicBarrier() {
		this.fCyclicBarrierMaintenanceRequest.reset();
	}

}
