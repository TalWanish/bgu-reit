import java.util.Map;
import java.util.Vector;
import java.util.concurrent.*;


public class Warehouse implements WarehouseInterface{
	
	
	private ConcurrentHashMap<String, RepairTool> fToolsMap;
	private ConcurrentHashMap<String, RepairMaterial> fMaterialsMap;
	
	public Warehouse(){
		this.fToolsMap=new ConcurrentHashMap<String, RepairTool>();
		this.fMaterialsMap=new ConcurrentHashMap<String,RepairMaterial>();

	}
	
	public Warehouse(Vector<RepairTool> vectorRepairTools,
			Vector<RepairMaterial> vectorRepairMaterials) {
	}

	public void addTool(RepairTool tool){
		fToolsMap.put(tool.getRepairToolName(),tool);
	}
	
	public void addMaterial(RepairMaterial material){
		fMaterialsMap.put(material.getName(),material);
	}
	
	@Override
	public void requestTool(RepairToolInformation repairToolInformation){
		System.out.println("Warehouse requestTool: Someone wants "+repairToolInformation.getToolName());
			RepairTool tempTool=fToolsMap.get(repairToolInformation.getToolName());
			tempTool.acquireTool(repairToolInformation.getRequestedQuantity());
			
	}
	
	//Subtracts the the amount of released tools from the current used tools 
	public void releaseTool(RepairToolInformation repairToolInformation){	
		
		System.out.println("Warehouse releaseTool: Someone wants to release "+repairToolInformation.getToolName());

		RepairTool tempTool=fToolsMap.get(repairToolInformation.getToolName());
		tempTool.releaseTool(repairToolInformation.getRequestedQuantity());
	
	}	
	
	public void printMaterialMap(){
		
		if(fMaterialsMap.isEmpty())
			System.out.println("The map is empty");
		for (Map.Entry<String, RepairMaterial> entry : fMaterialsMap.entrySet()) {
		    System.out.println(entry.getKey());
		}
		
	}

	@Override
	public void requestMaterial(RepairMaterialInformation repairMaterialInformation){
		System.out.println("Warehouse requestMaterial: Someone wants "+repairMaterialInformation.getMaterialName());

		RepairMaterial tempMaterial=fMaterialsMap.get(repairMaterialInformation.getMaterialName());
		tempMaterial.acquireMaterial(repairMaterialInformation.getRequestedQuantity());			
	}

	@Override
	public void updateToolStatistics(RepairToolInformation a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateMaterialStatistics(RepairMaterialInformation a) {
		// TODO Auto-generated method stub
		
	}






	
	
	
}
