import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.Iterator;


public class RunnableCustomerGroupManager implements Runnable{
	
	private CustomerGroupDetails fCustomerGroupDetails;
	private String fManagerName;
	private Management fManagement; 
	private double fTotalDamage=0;


	public RunnableCustomerGroupManager(String fManagerName,CustomerGroupDetails customerGroupDetails,Management fManagement){
		this.fManagerName=fManagerName;
		this.fCustomerGroupDetails = customerGroupDetails;
		this.fManagement=fManagement;
		}
	
	public void run() {
		
		
		System.out.println(fManagerName+" has RentalRequest? "+fCustomerGroupDetails.hasRentalRequest());

		while(fCustomerGroupDetails.hasRentalRequest()){
			
			RentalRequest currentRentalRequest=fCustomerGroupDetails.nextRequest();
			System.out.println(fManagerName+" has RentalRequest: "+currentRentalRequest.getRentalRequestType());

			fManagement.submitRentalRequest(currentRentalRequest);
			
			synchronized (currentRentalRequest) {
				try {
					currentRentalRequest.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
	
			
			System.out.println(fManagerName+" managed to fulfill Rental Request");

			currentRentalRequest.updateRentalRequestStatus("INPROGRESS");
			currentRentalRequest.updateRentalRequestAssetStatus("OCCUPIED");
			
			try {
				System.out.println(fManagerName+" should sleep for "+currentRentalRequest.getRentalRequestDuration());
				Thread.sleep(currentRentalRequest.getRentalRequestDuration());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			simulateStayInAsset();
			currentRentalRequest.updateRentalRequestAssetStatus("UNAVAILABLE");
			currentRentalRequest.updateRentalRequestStatus("COMPLETE");
			System.out.println(fManagerName+" creates DamageReport");
			createDamageReport(currentRentalRequest);
			System.out.println(fManagerName+" has RentalRequest?"+fCustomerGroupDetails.hasRentalRequest());

		}
		
	}

	private void createDamageReport(RentalRequest currentRentalRequest) {
		fManagement.addDamageReport(new DamageReport(currentRentalRequest.getRentalRequestAsset(),this.fTotalDamage));
		
	}
	
	
	private void simulateStayInAsset(){
		
		System.out.println(fManagerName+" simulateStayInAsset");

		Iterator<Customer> it_fCustomer=fCustomerGroupDetails.getCustomerArrayListIterator();
		
        ExecutorService stayInAssetExecutor = Executors.newFixedThreadPool(fCustomerGroupDetails.getCustomerArrayListSize());
        ExecutorCompletionService<Double> simulateStayInAsset=new ExecutorCompletionService<Double>(stayInAssetExecutor);
    	
		while(it_fCustomer.hasNext())
			simulateStayInAsset.submit(new CallableSimulateStayInAsset(it_fCustomer.next()));
		
		
		for(int i=0;i<fCustomerGroupDetails.getCustomerArrayListSize();i++){
			try {
				this.fTotalDamage=this.fTotalDamage+simulateStayInAsset.take().get();
				System.out.println(fManagerName+" Total damage for "+i+" customers is "+this.fTotalDamage);

			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println(fManagerName+" finished simulateStayInAsset. Total damage is "+this.fTotalDamage);

		
	}
	
	
	
	
	}
