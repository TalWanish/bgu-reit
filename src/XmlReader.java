import javax.xml.parsers.*;
import java.util.*;
import java.io.*;
import org.w3c.dom.*;

public class XmlReader {

	private static Object customerName;


	public XmlReader() {
	}

	// parse the customer groups file and create collection of group customer
	// details
	public static Vector<CustomerGroupDetails> parseCustomersGroups(String fileName) {
		// this vector will return
		Vector<CustomerGroupDetails> vectorCustomerGroupDetails = new Vector<CustomerGroupDetails>();

		try {
			File groupDetail = new File("./" + fileName + ".xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(groupDetail);
			doc.getDocumentElement().normalize();

			// initialize the groupDetail section to a NodeList
			NodeList customerGroupDetailsList = doc.getElementsByTagName("CustomerGroupDetails");

			for (int section = 0; section < customerGroupDetailsList.getLength(); section++) {
				Node customerGroupDetailsNode = customerGroupDetailsList.item(section);
				
				// create the collection to hold the rentalRequest
				Vector<RentalRequest> vectorRentalRequests = new Vector<RentalRequest>();
				
				// the vector thT holds the customers in the group
				Vector<Customer> customerVector = new Vector<Customer>();
				
				//initialize the group manager to empty string
				String managerName = "";

				if (customerGroupDetailsNode.getNodeType() == Node.ELEMENT_NODE) {
					Element customerGroupDetailElement = (Element) customerGroupDetailsNode;
					// Read the group managers' name
					managerName = customerGroupDetailElement.getElementsByTagName("GroupManagerName").item(0).getTextContent();
					
					NodeList customersList = customerGroupDetailElement.getElementsByTagName("Customers").item(0).getChildNodes();
					// Run over the customer section and create collection of customers
					for (int customerSection = 0; customerSection < customersList.getLength(); customerSection++) {
						Node customerNode = customersList.item(customerSection);
						if (customerNode.getNodeType() == Node.ELEMENT_NODE) {
							Element customerElement = (Element) customerNode;
							// create customer attribute
							String customerName = customerElement.getElementsByTagName("Name").item(0).getTextContent();
							Customer.Vandalism vandalismType = compareStringToVandalism(customerElement.getElementsByTagName("Vandalism").item(0).getTextContent());
							double minimum = Integer.parseInt(customerElement.getElementsByTagName("MinimumDamage").item(0).getTextContent());
							double maximum = Integer.parseInt(customerElement.getElementsByTagName("MaximumDamage").item(0).getTextContent());
							// create the customer and add to collection
							customerVector.add(new Customer(customerName,vandalismType, minimum, maximum));
						}
					}
					
					// create list of requests
					NodeList requestList = customerGroupDetailElement.getElementsByTagName("RentalRequests").item(0).getChildNodes();
					// run over the rental request section and create collection of rentalRequests
					for (int requestSection = 0; requestSection < requestList.getLength(); requestSection++) {
						Node requestNode = requestList.item(requestSection);
						if (requestNode.getNodeType() == Node.ELEMENT_NODE) {
							Element requestElement = (Element) requestNode;
							// initialize rental request attributes
							int id = Integer.parseInt(requestNode.getAttributes().getNamedItem("id").getTextContent());
							String type = requestElement.getElementsByTagName("Type").item(0).getTextContent();
							double size = Double.parseDouble(requestElement.getElementsByTagName("Size").item(0).getTextContent());
							int durationOfStay = Integer.parseInt(requestElement.getElementsByTagName("Duration").item(0).getTextContent());
							// create new rentalRequest and add the request to the collection
							vectorRentalRequests.add(new RentalRequest(id,type, size, durationOfStay));
						}
					}
				}
				vectorCustomerGroupDetails.add(new CustomerGroupDetails(vectorRentalRequests, customerVector, managerName));
			}
		} catch (Exception e) {e.printStackTrace();
		}
		return vectorCustomerGroupDetails;
	}

	// parse the AssetContentsRepairDetails file and create collection of RepairToolInformation ;
	public static Vector<RepairToolInformation> parseAssetContentsRepairTool(String fileName) {
		Vector<RepairToolInformation> vectorRepairToolInformation = new Vector<RepairToolInformation>();
		try {
			File repairTool = new File("./" + fileName + ".xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(repairTool);
			doc.getDocumentElement().normalize();
			
			// initialize the AssetContent section to list
			NodeList AssetContentList = doc.getElementsByTagName("AssetContent");
			for (int section = 0; section < AssetContentList.getLength(); section++) {
				Node assetContentNode = AssetContentList.item(section);
				
				// the name of the content
				String ContentName = "";
				
				// vector of the repair tool
				Vector<RepairTool> vectorRepairTool = new Vector<RepairTool>();
				if (assetContentNode.getNodeType() == Node.ELEMENT_NODE) {
					Element contentElement = (Element) assetContentNode;
					
					// initialize the name of content
					ContentName = contentElement.getElementsByTagName("Name").item(0).getTextContent();
					
					// create Node of tools
					Node toolsinfo = contentElement.getElementsByTagName("Tools").item(0);
					
					// create list of tool information
					NodeList toolinfo = toolsinfo.getChildNodes();
					
					// for every tool initialize repairToolInformation
					for (int section2 = 0; section2 < toolinfo.getLength(); section2++) {
						Node toolNode = toolinfo.item(section2);
						if (toolNode.getNodeType() == Node.ELEMENT_NODE) {
							Element toolElement = (Element) toolNode;
							
							// initialize the name of the tool
							String toolName = toolElement.getElementsByTagName("Name").item(0).getTextContent();
							
							// initialize the quantity of tool
							int quantity = Integer.parseInt(toolElement.getElementsByTagName("Quantity").item(0).getTextContent());
							
							// add repair tool to the vector of repair tool information
							vectorRepairTool.add(new RepairTool(toolName,quantity));
						}
					}
				}
				vectorRepairToolInformation.add(new RepairToolInformation(ContentName, vectorRepairTool));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vectorRepairToolInformation;
	}
	
	// parse the AssetContentsRepairDetails file and create collection of RepairMaterialInformation;
	public static Vector<RepairMaterialInformation> parseAssetContentsRepairMaterial(String fileName) {
		Vector<RepairMaterialInformation> vectorRepairMaterialInformation = new Vector<RepairMaterialInformation>();
		try {
			File repairMaterial = new File("./" + fileName + ".xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(repairMaterial);
			doc.getDocumentElement().normalize();
			
			// initialize the AssetContent section to list
			NodeList AssetContentList = doc.getElementsByTagName("AssetContent");
			for (int section = 0; section < AssetContentList.getLength(); section++) {
				Node assetContentNode = AssetContentList.item(section);
				
				//Name of the Asset content item
				String ContentName = "";
				
				// vector of repair material
				Vector<RepairMaterial> vectorRepairMaterial = new Vector<RepairMaterial>();
				if (assetContentNode.getNodeType() == Node.ELEMENT_NODE) {
					Element contentElement = (Element) assetContentNode;
					
					// initialize the name of content
					ContentName = contentElement.getElementsByTagName("Name").item(0).getTextContent();
					
					// create Node of materials
					Node materialsinfo = contentElement.getElementsByTagName("Materials").item(0);
					
					// create list of material information
					NodeList materialinfo = materialsinfo.getChildNodes();
					
					// for every material initialize repairToolInformation
					for (int section2 = 0; section2 < materialinfo.getLength(); section2++) {
						Node materialNode = materialinfo.item(section2);
						if (materialNode.getNodeType() == Node.ELEMENT_NODE) {
							Element materialElement = (Element) materialNode;
							
							// initialize the name of the material
							String materialName = materialElement.getElementsByTagName("Name").item(0).getTextContent();
							
							// initialize the quantity of material
							int quantity = Integer.parseInt(materialElement.getElementsByTagName("Quantity").item(0).getTextContent());
							
							// add repair material to the vector of repair material information
							vectorRepairMaterial.add(new RepairMaterial(materialName, quantity));
						}
					}
				}
				vectorRepairMaterialInformation.add(new RepairMaterialInformation(ContentName,vectorRepairMaterial));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vectorRepairMaterialInformation;
	}

	// Parse the assets file and create the assets
	public static Assets parseAssets(String fileName) {
		int assetName = 0;
		Vector<Asset> assets = new Vector<Asset>();
		try {
			File assetFile = new File("./" + fileName + ".xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(assetFile);
			doc.getDocumentElement().normalize();
			
			// Initialize the asset section to list
			NodeList assetList = doc.getElementsByTagName("Asset");
			for (int section = 0; section < assetList.getLength(); section++) {
				Node assetNode = assetList.item(section);
				if (assetNode.getNodeType() == Node.ELEMENT_NODE) {
					Element aElement = (Element) assetNode;
					
					// Initialize asset contents list
					NodeList assetContents = aElement.getElementsByTagName("AssetContents").item(0).getChildNodes();
					
					// vector contains all the content of a specific asset
					Vector<AssetContent> vectorContents = new Vector<AssetContent>();
					for (int index = 0; index < assetContents.getLength(); index++) {
						Node content = assetContents.item(index);
						if (content.getNodeType() == Node.ELEMENT_NODE) {
							Element contentElement = (Element) content;
							String contentName = contentElement.getElementsByTagName("Name").item(0).getTextContent();
							double repairCostMultiplier = Double.parseDouble(contentElement.getElementsByTagName("RepairMultiplier").item(0).getTextContent());
							
							// create a new asset and push into the vector
							AssetContent assetContent = new AssetContent(contentName, repairCostMultiplier);vectorContents.add(assetContent);
						}
					}
					
					String type = aElement.getElementsByTagName("Type").item(0).getTextContent();
					double costPerNight = Double.parseDouble(aElement.getElementsByTagName("CostPerNight").item(0).getTextContent());
					double size = Double.parseDouble(aElement.getElementsByTagName("Size").item(0).getTextContent());
					
					// Initialize location
					Node location = aElement.getElementsByTagName("Location").item(0);
					double x = Double.parseDouble(location.getAttributes().getNamedItem("x").getTextContent());
					double y = Double.parseDouble(location.getAttributes().getNamedItem("y").getTextContent());
					Location locationAsset = new Location(x, y);
					Asset asset = new Asset(assetName + "", type,locationAsset, vectorContents, AssetStatus.AVAILABLE, costPerNight, size);
					assets.add(asset);
				}
				assetName++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Assets(assets);
	}

	// Parse the InitialData file and return the number of maintenance
	public static int pharseNumberOfMaintenancePersons(String fileName) {
		int numMantenance = -1;
		try {
			File InitialData = new File("./" + fileName + ".xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(InitialData);
			doc.getDocumentElement().normalize();
			
			// initialize the InitialData section to node
			Node maintenanceNode = doc.getElementsByTagName("NumberOfMaintenancePersons").item(0);
			if (maintenanceNode.getNodeType() == Node.ELEMENT_NODE) {
				Element maintenanceElement = (Element) maintenanceNode;
				numMantenance = Integer.parseInt(maintenanceElement.getTextContent());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return numMantenance;
	}

	// Parse the InitialData file and return the amount of rental requests
	public static int pharseTotalNumberOfRentalRequests(String fileName) {
		int rentalAmount = -1;
		try {
			File InitialData = new File("./" + fileName + ".xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(InitialData);
			doc.getDocumentElement().normalize();
			
			// initialize the InitialData section to node
			Node renatalNnode = doc.getElementsByTagName("TotalNumberOfRentalRequests").item(0);
			if (renatalNnode.getNodeType() == Node.ELEMENT_NODE) {
				Element rentalElement = (Element) renatalNnode;
				rentalAmount = Integer.parseInt(rentalElement.getTextContent());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rentalAmount;
	}

	public static Warehouse parseWarehouse(String fileName) {
		
		// vector of repair tool
		Vector<RepairTool> vectorRepairTools = new Vector<RepairTool>();
		
		// vector of material
		Vector<RepairMaterial> vectorRepairMaterials = new Vector<RepairMaterial>();

		try {
			File InitialData = new File("./" + fileName + ".xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(InitialData);
			doc.getDocumentElement().normalize();
			
			// Initialize the InitialData section to node
			NodeList repairToolList = doc.getElementsByTagName("Tools").item(0).getChildNodes();

			// Create all repair tools
			for (int toolsection = 0; toolsection < repairToolList.getLength(); toolsection++) {
				Node repairToolNode = repairToolList.item(toolsection);

				if (repairToolNode.getNodeType() == Node.ELEMENT_NODE) {
					Element repairToolElement = (Element) repairToolNode;
					
					// Create repair tool attribute
					String name = repairToolElement.getElementsByTagName("name").item(0).getTextContent();
					int quantity = Integer.parseInt(repairToolElement.getElementsByTagName("quantity").item(0).getTextContent());
					vectorRepairTools.add(new RepairTool(name, quantity));
				}
			}

			// Initialize the InitialData section to node
			NodeList repairMetarialList = doc.getElementsByTagName("Materials").item(0).getChildNodes();
			
			// Create all material
			for (int metarialsection = 0; metarialsection < repairToolList.getLength(); metarialsection++) {
				Node repairMaterialNode = repairMetarialList.item(metarialsection);

				if (repairMaterialNode.getNodeType() == Node.ELEMENT_NODE) {
					Element repairMaterialElement = (Element) repairMaterialNode;
					
					// Create repair material attribute
					String name = repairMaterialElement.getElementsByTagName("name").item(0).getTextContent();
					int quantity = Integer.parseInt(repairMaterialElement.getElementsByTagName("quantity").item(0).getTextContent());
					vectorRepairMaterials.add(new RepairMaterial(name, quantity));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Warehouse(vectorRepairTools, vectorRepairMaterials);
	}

	//Parse InitialData file and return collection of clerkDetails
	public static <ClerkDetails> Vector<ClerkDetails> parseClerkDetails(String fileName) {
		Vector<ClerkDetails> vectorClerkDetails = new Vector<ClerkDetails>();
		try {
			File InitialData = new File("./" + fileName + ".xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(InitialData);
			doc.getDocumentElement().normalize();

			// Initialize the InitialData section to node
			NodeList clerckDetailsList = doc.getElementsByTagName("Clerks").item(0).getChildNodes();

			// Create all clerckDetails
			for (int section = 0; section < clerckDetailsList.getLength(); section++) {
				Node clerckDetaildNode = clerckDetailsList.item(section);

				if (clerckDetaildNode.getNodeType() == Node.ELEMENT_NODE) {
					Element clerkDetailsElement = (Element) clerckDetaildNode;
					
					// create clerkDetails attribute
					String name = clerkDetailsElement.getElementsByTagName("name").item(0).getTextContent();
					
					// Initialize the location
					Node location = clerkDetailsElement.getElementsByTagName("Location").item(0);
					double x = Double.parseDouble(location.getAttributes().getNamedItem("x").getTextContent());
					double y = Double.parseDouble(location.getAttributes().getNamedItem("y").getTextContent());
					Location locationClerk = new Location(x, y);
					
					// add the clerckdeTail object to the collection
					vectorClerkDetails.add(new ClerkDetailss(name, locationClerk));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vectorClerkDetails;
	}

	// figure enum out
	private static Customer.Vandalism compareStringToVandalism(String vandalismType) {
		if (vandalismType.equals(Customer.Vandalism.Arbitrary.toString()))
			return Customer.Vandalism.Arbitrary;
		else if (vandalismType.equals(Customer.Vandalism.Fixed.toString()))
			return Customer.Vandalism.Fixed;
		else
			return Customer.Vandalism.None;
	}
}
