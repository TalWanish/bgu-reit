
public enum VandalismStatus{
	
	    ARBITRARY("ARBITRARY"),
	    FIXED("FIXED"),
	    NONE("NONE");
	
	    private final String fVandalismStatus;
	    
	    VandalismStatus(String fVandalismStatus){
	    	this.fVandalismStatus = fVandalismStatus;	
	    }

		public String getVandalismType() {
			return fVandalismStatus;
		}
	}