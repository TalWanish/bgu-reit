import java.util.LinkedList;


public class AssetContentRepairDetails {
	
	private String fAssentContentName;
	private LinkedList<RepairToolInformation> fListOfRequiredRepairTools=new LinkedList<RepairToolInformation>();
	private LinkedList<RepairMaterialInformation> fListOfRequiredRepairMaterials=new LinkedList<RepairMaterialInformation>();

	public AssetContentRepairDetails(String fAssentContentName){
		this.fAssentContentName=fAssentContentName;
	}
	
	public void addRepairToolInformation(String toolName,int quantity){
		fListOfRequiredRepairTools.add(new RepairToolInformation(toolName, quantity));
	}
	
	public void addRepairMaterialInformation(String materialName,int quantity){
		fListOfRequiredRepairMaterials.add(new RepairMaterialInformation(materialName, quantity));
	}

	public String getAssentContentName() {
		return fAssentContentName;
	}
	
	public LinkedList<RepairToolInformation> getListOfRequiredRepairTools(){
		
		return this.fListOfRequiredRepairTools;
	}

	public LinkedList<RepairMaterialInformation> getListOfRequiredRepairMaterials() {
		return this.fListOfRequiredRepairMaterials;

	}
}
