import java.util.Vector;


public class RepairToolMaterialStatistics implements Statistical{
	
	private String fRepairName;
	private int fTotalQuantity;
	private int fTotalAcquirements=0;
	private Vector<String> fStatisticalInformation=new Vector<String>(); 
	
	
	public RepairToolMaterialStatistics(String fRepairName, int fTotalQuantity) {
		this.fRepairName=fRepairName;
		this.fTotalQuantity=fTotalQuantity;
}

	@Override
	public void updateStatistcs(String update) {
		System.out.println("RepairToolMaterialStatistic updateStatistcss for "+this.fRepairName+" : "+update);
		fStatisticalInformation.addElement(update);	
		fTotalAcquirements++;
	}

	
	@Override
	public String getItemStatistcs() {
		String statistcs="Repair item information: "+fRepairName+" Total Quantity: "+
	fTotalQuantity+" Total Acquirements: "+fTotalAcquirements;
		for(String it: fStatisticalInformation)
			statistcs=statistcs+'\n'+it;
		
		return statistcs;

	}

}
