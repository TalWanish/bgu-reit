
public class AssetContent {
		
	private String fName;
	private double fHealth;
	private double fRepairCostMultiplier;
	
	
	public AssetContent(String fName,double fRepairCostMultiplier){
		this.fName=fName;
		this.fRepairCostMultiplier=fRepairCostMultiplier;
		fHealth=100;
	}
	
	public void markAssetContentsAsFix(){
		fHealth=100;
	}
	
	public boolean isAssetContentNeedToBeFixed(){
		return fHealth<65;
	}

	public double getAssetContentHealth() {
		return fHealth;
	}

	public double getRepairCostMultiplier() {
		return fRepairCostMultiplier;
	}
	
	public double getRepairTime(){
		return (100-fHealth)*fRepairCostMultiplier;
	}

	public String getAssetContentName() {
		return fName;
	}


	
	
	

}
