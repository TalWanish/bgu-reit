import java.util.Vector;


public class RepairMaterialInformation {

	String fMaterialName;
	int fQuantity;
	
	public RepairMaterialInformation(){
		fMaterialName="";
		fQuantity=0;
	}
	
	public RepairMaterialInformation(String fMaterialName,int fQuantity){
		this.fMaterialName=fMaterialName;
		this.fQuantity=fQuantity;
	}

	public String getMaterialName() {
		return fMaterialName;
	}


	public int getRequestedQuantity() {
		return fQuantity;
	}

	
	


}
