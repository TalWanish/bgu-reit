import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

public class Assets {
	
	
	private ConcurrentHashMap<String, Asset> fAssetMap=new ConcurrentHashMap<String, Asset>();
	private ConcurrentHashMap<Integer, Asset> fAssetMapBySize=new ConcurrentHashMap<Integer, Asset>();
	private ConcurrentHashMap<String, ConcurrentHashMap<Integer, Asset>> fAssetMapOfMapsByType=new ConcurrentHashMap<String, ConcurrentHashMap<Integer, Asset>>();
	private Vector<Asset> fDamagedAssetsVec=new Vector<Asset>();
	
	public Assets(){
		
	}

	public void addToAssets(Asset assetToAdd) {
		
		fAssetMap.put(assetToAdd.getAssetName(),assetToAdd);

		Integer size = assetToAdd.getAssetSize();
		String type = assetToAdd.getAssetName();

		// look for type in map, if type exist, add to map of sizes, else add to
		// both maps
		// maybe change to put if absent
		if (fAssetMapOfMapsByType.containsKey(type)) {
			fAssetMapOfMapsByType.get(type).put(size, assetToAdd);
		} else {
			fAssetMapOfMapsByType.put(type, fAssetMapBySize).put(size,
					assetToAdd);
		}
	}

	public Asset findSuitableAsset(String type, Integer size) {
		return fAssetMapOfMapsByType.get(type).get(size);
	}

	public Vector<Asset> retrieveDamagedAssetsVec(){
		this.fDamagedAssetsVec.clear();
		for(Asset it: fAssetMapBySize.values()){
			if(it.getAssetHealth()<65)
				fDamagedAssetsVec.addElement(it);
		}
		
		return this.fDamagedAssetsVec;
		
	}

	public void updateAssetsHealth(Asset asset, double damage) {
		Asset tempAsset=this.fAssetMap.get(asset.getAssetName());
		tempAsset.updateHealth(damage);
	}
}
